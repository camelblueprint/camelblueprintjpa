Camel Router Project for Blueprint (OSGi)
=========================================

To build this project use

    mvn install

To run the project you can execute the following Maven goal

    mvn camel:run

To deploy the project in OSGi. For example using Apache ServiceMix
or Apache Karaf. You can run the following command from its shell:

    osgi:install -s mvn:com.localhost/bluePrint/0.0.1-SNAPSHOT

For more help see the Apache Camel documentation

    http://camel.apache.org/
    
# Camel Servlet REST and OSGi Blueprint example

### Introduction
This example shows how to use Servlet REST to define REST endpoints in Camel routes using the Rest DSL


### Build
You will need to compile this example first:

	mvn install


### Run
To install Apache Camel in Karaf you type in the shell (we use version ${project.version}):

	feature:repo-add camel ${project.version} 2.21.0
	feature:install camel

First you need to install the following features in Karaf/ServiceMix with:

	feature:install camel-servlet
	feature:install camel-jackson
	feature:install war
	feature:install camel-activemq
	feature:install camel-jpa
	feature:install jpa
	feature:install transaction
	** install -s mvn:org.apache.geronimo.specs/geronimo-jpa_2.0_spec/1.1
	** features:install hibernate
	** feature:install hibernate-orm
	

Then you can install the Camel example:

	install -s mvn:org.apache.camel.example/camel-example-servlet-rest-blueprint/${project.version}

And you can see the application running by tailing the logs

	log:tail

And you can use <kbd>ctrl</kbd>+<kbd>c</kbd> to stop tailing the log.

There is a user REST service that supports the following operations

 - GET /user/{id} - to view a user with the given id
 - GET /user/final - to view all users
 - PUT /user - to update/create an user

The view operations are HTTP GET, and update is using HTTP PUT.

From a web browser you can access the first two services using the following links

      http://localhost:8181/camel-example-servlet-rest-blueprint/rest/user/123    - to view the user with id 123
      http://localhost:8181/camel-example-servlet-rest-blueprint/rest/user/findAll   - to list all users

From the command shell you can use curl to access the service as shown below:

    curl -X GET -H "Accept: application/json" http://localhost:8181/camel-example-servlet-rest-blueprint/rest/user/123
    curl -X GET -H "Accept: application/json" http://localhost:8181/camel-example-servlet-rest-blueprint/rest/user/findAll
    curl -X PUT -d "{ \"id\": 666, \"name\": \"The devil\"}" -H "Accept: application/json" http://localhost:8181/camel-example-servlet-rest-blueprint/rest/user


### Configuration
This example is implemented in XML DSL in the `src/main/resources/OSGI-INF/blueprint/camel.xml` file.


### Forum, Help, etc

If you hit problems please let us know on the Camel Forums
	<http://camel.apache.org/discussion-forums.html>

Please help us make Apache Camel better - we appreciate any feedback you may
have.  Enjoy!
