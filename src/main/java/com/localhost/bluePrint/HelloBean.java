package com.localhost.bluePrint;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.hibernate.SessionFactory;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A bean which we use in the route
 */
public class HelloBean implements Processor {

//    SessionFactory sf;

    private String say = "Hello World";

    @Override
    public void process(Exchange exchange) throws Exception {
        ObjectMapper om = new ObjectMapper();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Response response = new Response();
        response.setCadena("Hola" + " at " + "Jhonatan");
        response.setAhora(new Date());

        exchange.getOut().setHeader("Content-Type", "application/json");
        exchange.getOut().setHeader("Accept", "*/*");
        exchange.getOut().setHeader(Exchange.HTTP_METHOD, "POST");
        exchange.getOut().setBody(om.writeValueAsString(response));
    }

    // public IMensajeService getMensajeService() {
    // return mensajeService;
    // }
    //
    // public void setMensajeService(IMensajeService mensajeService) {
    // this.mensajeService = mensajeService;
    // }

}

class Response {
    private String cadena;
    private Date ahora;

    public String getCadena() {
        return cadena;
    }

    public void setCadena(String cadena) {
        this.cadena = cadena;
    }

    public Date getAhora() {
        return ahora;
    }

    public void setAhora(Date ahora) {
        this.ahora = ahora;
    }

}
