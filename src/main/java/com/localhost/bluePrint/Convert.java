/*
 *
 * Copyright (c) 2017, Multiva SA de CV
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are not permitted.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * Developed 2017 by Legosoft www.legosoft.com.mx
 *
 */

package com.localhost.bluePrint;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

/**
 * @company : Legosoft Sa de CV
 * @author : Ing. Raul Gutierrez Molina
 * @email : rgutierrez@legosoft.com.mx
 * @project : bluePrint
 * @package : com.localhost.bluePrint
 * @class : Convert.java
 * @version : 1.0.0
 * @since : 15/01/2019
 * @doc :
 *
 */

public class Convert implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		System.out.println("QUE HAY ACA ");
		Persona p = exchange.getIn().getBody(Persona.class);
		System.out.println(p.getNombre());

	}

}
